/**
 * Perform a key press
 * @param  {String}   key  The key to press
 */
export default (key, platform) => {

    let platform2;

  switch (platform) {
      case myChromeBrowser.constructor.name:
          platform2 = myChromeBrowser;
          break;
      case myAppiumAppr.constructor.name:
          platform2 = myAppiumAppr;
      default:
          platform2 = myChromeBrowser;
          break;
  }

  platform2.keys(key);
};
