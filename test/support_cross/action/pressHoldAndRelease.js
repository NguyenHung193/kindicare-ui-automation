/**
 * Perform a key press
 * @param  {String}   key  The key to press
 */
// export default (key) => {
//     browser.keys(key);
// };

export function holdDownKey(character, platform) {
  
  let platform2;

  switch (platform) {
      case myChromeBrowser.constructor.name:
          platform2 = myChromeBrowser;
          break;
      case myAppiumAppr.constructor.name:
          platform2 = myAppiumAppr;
      default:
          platform2 = myChromeBrowser;
          break;
  }

  platform2.performActions([{
      type: 'key',
      id: 'keyboard',
      actions: [{ type: 'keyDown', value: character }],
    }]);
  }
  
  export function releaseKey(character) {
    const keyAction = '';
    platform2.performActions([{
      type: 'key',
      id: 'keyboard',
      actions: [{ type: 'keyUp', value: character }],
    }]);
  }