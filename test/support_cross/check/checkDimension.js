import checkIfElementExists from '../lib/checkIfElementExists';
import {loginPage , random, homePage, loginAccionaPage, homeAccionaPage, myTimeSheetAccionaPage, plantInspectionAccionaPage, sideMenuAccionaPage, leaveRequestAccionaPage, eventsAccionaPage} from '../../pageObjects/exportPage'

/**
 * Check the dimensions of the given element
 * @param  {String}   selector         Element selector
 * @param  {String}   falseCase    Whether to check if the dimensions match or
 *                                 not
 * @param  {String}   expectedSize Expected size
 * @param  {String}   dimension    Dimension to check (broad or tall)
 */
export default (selector, falseCase, expectedSize, dimension, platform) => {

    let platform2;

    switch (platform) {
        case Object.keys({myChromeBrowser})[0]:
            platform2 = myChromeBrowser;
            break;
        case Object.keys({myAppiumAppr})[0]:
            platform2 = myAppiumAppr;
            break;
        default:
            platform2 = myChromeBrowser;
            break;
    }

    const selector2 = (((loginPage)[selector])||((homePage)[selector])||((loginAccionaPage)[selector])||((homeAccionaPage)[selector])||((myTimeSheetAccionaPage)[selector])||((plantInspectionAccionaPage)[selector])||((sideMenuAccionaPage)[selector])||((leaveRequestAccionaPage)[selector])||((eventsAccionaPage)[selector]));
    /**
     * The size of the given element
     * @type {Object}
     */
    const elementSize = platform2.$(selector2).getSize();

    /**
     * Parsed size to check for
     * @type {Int}
     */
    const intExpectedSize = parseInt(expectedSize, 10);

    /**
     * The size property to check against
     * @type {Int}
     */
    let originalSize = elementSize.height;

    /**
     * The label of the checked property
     * @type {String}
     */
    let label = 'height';

    if (dimension === 'broad') {
        originalSize = elementSize.width;
        label = 'width';
    }

    if (falseCase) {
        expect(originalSize).not.toBe(
            intExpectedSize,
            `Element "${selector2}" should not have a ${label} of `
            + `${intExpectedSize}px`
        );
    } else {
        expect(originalSize).toBe(
            intExpectedSize,
            `Element "${selector2}" should have a ${label} of `
            + `${intExpectedSize}px, but is ${originalSize}px`
        );
    }
};
