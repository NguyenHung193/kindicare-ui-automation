/**
 * Check if a modal was opened
 * @param  {String}   modalType  The type of modal that is expected (alertbox,
 *                               confirmbox or prompt)
 * @param  {String}   falseState Whether to check if the modal was opened or not
 */
export default (modalType, falseState, platform) => {

    let platform2;

    switch (platform) {
        case Object.keys({myChromeBrowser})[0]:
            platform2 = myChromeBrowser;
            break;
        case Object.keys({myAppiumAppr})[0]:
            platform2 = myAppiumAppr;
            break;
        default:
            platform2 = myChromeBrowser;
            break;
    }

    /**
     * The text of the prompt
     * @type {String}
     */
    let promptText = '';

    try {
        promptText = platform2.getAlertText();

        if (falseState) {
            expect(promptText).not.toEqual(
                null,
                `A ${modalType} was opened when it shouldn't`
            );
        }
    } catch (e) {
        if (!falseState) {
            expect(promptText).toEqual(
                null,
                `A ${modalType} was not opened when it should have been`
            );
        }
    }
};
