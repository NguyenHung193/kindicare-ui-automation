import checkIfElementExists from '../lib/checkIfElementExists';
import {loginPage , random, homePage, loginAccionaPage, homeAccionaPage, myTimeSheetAccionaPage, plantInspectionAccionaPage, sideMenuAccionaPage, leaveRequestAccionaPage, eventsAccionaPage} from '../../pageObjects/exportPage'

/**
 * Check if the given elements contains text
 * @param  {String}   elementType   Element type (element or button)
 * @param  {String}   selector       Element selector
 * @param  {String}   falseCase     Whether to check if the content contains
 *                                  text or not
 */
export default (elementType, selector, falseCase, platform) => {

    let platform2;

    switch (platform) {
        case Object.keys({myChromeBrowser})[0]:
            platform2 = myChromeBrowser;
            break;
        case Object.keys({myAppiumAppr})[0]:
            platform2 = myAppiumAppr;
            break;
        default:
            platform2 = myChromeBrowser;
            break;
    }

    const selector2 = (((loginPage)[selector])||((homePage)[selector])||((loginAccionaPage)[selector])||((homeAccionaPage)[selector])||((myTimeSheetAccionaPage)[selector])||((plantInspectionAccionaPage)[selector])||((sideMenuAccionaPage)[selector])||((leaveRequestAccionaPage)[selector])||((eventsAccionaPage)[selector]));
    /**
     * The command to perform on the browser object
     * @type {String}
     */
    let command = 'getValue';

    if (
        elementType === 'button'
        || platform2.$(selector2).getAttribute('value') === null
    ) {
        command = 'getText';
    }

    /**
     * False case
     * @type {Boolean}
     */
    let boolFalseCase;

    /**
     * The text of the element
     * @type {String}
     */
    const text = platform2.$(selector2)[command]();

    if (typeof falseCase === 'undefined') {
        boolFalseCase = false;
    } else {
        boolFalseCase = !!falseCase;
    }

    if (boolFalseCase) {
        expect(text).toBe('');
    } else {
        expect(text).not.toBe('');
    }
};
