import checkIfElementExists from '../lib/checkIfElementExists';
import {loginPage , random, homePage, loginAccionaPage, homeAccionaPage, myTimeSheetAccionaPage, plantInspectionAccionaPage, sideMenuAccionaPage, leaveRequestAccionaPage, eventsAccionaPage} from '../../pageObjects/exportPage'

/**
 * Check the given property of the given element
 * @param  {String}   isCSS         Whether to check for a CSS property or an
 *                                  attribute
 * @param  {String}   attrName      The name of the attribute to check
 * @param  {String}   selector          Element selector
 * @param  {String}   falseCase     Whether to check if the value of the
 *                                  attribute matches or not
 * @param  {String}   expectedValue The value to match against
 */
export default (isCSS, attrName, selector, falseCase, expectedValue, platform) => {

    let platform2;

    switch (platform) {
        case Object.keys({myChromeBrowser})[0]:
            platform2 = myChromeBrowser;
            break;
        case Object.keys({myAppiumAppr})[0]:
            platform2 = myAppiumAppr;
            break;
        default:
            platform2 = myChromeBrowser;
            break;
    }

    const selector2 = (((loginPage)[selector])||((homePage)[selector])||((loginAccionaPage)[selector])||((homeAccionaPage)[selector])||((myTimeSheetAccionaPage)[selector])||((plantInspectionAccionaPage)[selector])||((sideMenuAccionaPage)[selector])||((leaveRequestAccionaPage)[selector])||((eventsAccionaPage)[selector]));
    /**
     * The command to use for fetching the expected value
     * @type {String}
     */
    const command = isCSS ? 'getCSSProperty' : 'getAttribute';

    /**
     * Te label to identify the attribute by
     * @type {String}
     */
    const attrType = (isCSS ? 'CSS attribute' : 'Attribute');

    /**
     * The actual attribute value
     * @type {Mixed}
     */
    let attributeValue = platform2.$(selector)[command](attrName);

    // eslint-disable-next-line
    expectedValue = isFinite(expectedValue) ?
        parseFloat(expectedValue)
        : expectedValue;

    /**
     * when getting something with a color or font-weight WebdriverIO returns a
     * object but we want to assert against a string
     */
    if (attrName.match(/(color|font-weight)/)) {
        attributeValue = attributeValue.value;
    }
    if (falseCase) {
        expect(attributeValue).not.toEqual(
            expectedValue,
            `${attrType}: ${attrName} of element "${selector2}" should `
            + `not contain "${attributeValue}"`
        );
    } else {
        expect(attributeValue).toEqual(
            expectedValue,
            `${attrType}: ${attrName} of element "${selector2}" should `
            + `contain "${attributeValue}", but "${expectedValue}"`
        );
    }
};
