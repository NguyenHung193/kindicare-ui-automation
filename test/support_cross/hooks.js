const video = require('wdio-video-reporter');
const { generate } = require('multiple-cucumber-html-reporter');
const { removeSync } = require('fs-extra');
const { ReportAggregator, HtmlReporter} = require('@rpii/wdio-html-reporter');
const log4js = require('@log4js-node/log4js-api');
const logger = log4js.getLogger('default');
const reportportal = require('wdio-reportportal-reporter');

const cucumberJson = require('wdio-cucumberjs-json-reporter').default;


// const conf = {
//     reportPortalClientConfig: { // report portal settings
//       token: '00000000-0000-0000-0000-00000000000',
//       endpoint: 'https://reportportal-url/api/v1',
//       launch: 'launch_name',
//       project: 'project_name',
//       mode: 'DEFAULT',
//       debug: false,
//       description: "Launch description text",
//       attributes: [{key:"tag", value: "foo"}],
//       headers: {"foo": "bar"} // optional headers for internal http client
//     },
//     reportSeleniumCommands: false, // add selenium commands to log
//     seleniumCommandsLogLevel: 'debug', // log level for selenium commands
//     autoAttachScreenshots: false, // automatically add screenshots
//     screenshotsLogLevel: 'info', // log level for screenshots
//     parseTagsFromTestTitle: false, // parse strings like `@foo` from titles and add to Report Portal
//     cucumberNestedSteps: false, // report cucumber steps as Report Portal steps
//     autoAttachCucumberFeatureToScenario: false, // requires cucumberNestedSteps to be true for use
//     isSauseLabRun: false // automatically add SauseLab ID to rp tags.
//   };

//
// =====
// Hooks
// =====
// WebdriverIO provides a several hooks you can use to interfere the test process in order to
// enhance it and build services around it. You can either apply a single function to it or
// an array of methods. If one of them returns with a promise,
// WebdriverIO will wait until that promise is resolved to continue.
//
exports.hooks = {
    /**
     * Gets executed once before all workers get launched.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     */
    onPrepare: function (config, capabilities, browser, done) {
        // removeSync('report/zipFolder/');
        // removeSync('report/jsonFile/');
        // removeSync('report/report/');
        // video.start(browser, done)
        // let reportAggregator = new ReportAggregator({
        //     outputDir: './reports/html-reports/',
        //     filename: 'master-report.html',
        //     reportTitle: 'Master Report',
        //     browserName : capabilities.browserName,
        //     collapseTests: true,
        //     // to use the template override option, can point to your own file in the test project:
        //     // templateFilename: path.resolve(__dirname, '../template/wdio-html-reporter-alt-template.hbs')
        // });
        // reportAggregator.clean() ;
 
        // global.reportAggregator = reportAggregator;
    },
    /**
     * Gets executed before a worker process is spawned & can be used to initialize specific service
     * for that worker as well as modify runtime environments in an async fashion.
     * @param  {String} cid    capability id (e.g 0-0)
     * @param  {[type]} caps   object containing capabilities for session
     * @param  {[type]} specs  specs to be run in the worker process
     * @param  {[type]} args   object that will be merged with the main
     *                         configuration once worker is initialized
     * @param  {[type]} execArgv list of string arguments passed to the worker process
     */
    // onWorkerStart: function (cid, caps, specs, args, execArgv) {
    // },
    /**
     * Gets executed just before initializing the webdriver session and test framework.
     * It allows you to manipulate configurations depending on the capability or spec.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that are to be run
     */
    // beforeSession: function (config, capabilities, specs) {
    // },
    /**
     * Gets executed before test execution begins. At this point you can access to all global
     * variables like `browser`. It is the perfect place to define custom commands.
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that are to be run
     */
    // before: function (capabilities, specs) {
    // },
    /**
     * Gets executed before the suite starts.
     * @param {Object} suite suite details
     */
    // beforeSuite: function (suite) {
    // },
    /**
     * This hook gets executed _before_ every hook within the suite starts.
     * (For example, this runs before calling `before`, `beforeEach`, `after`)
     *
     * (`stepData` and `world` are Cucumber-specific.)
     *
     */
    // beforeHook: function (test, context, stepData, world) {
    // },
    /**
     * Hook that gets executed _after_ every hook within the suite ends.
     * (For example, this runs after calling `before`, `beforeEach`, `after`, `afterEach` in Mocha.)
     *
     * (`stepData` and `world` are Cucumber-specific.)
     */
    // afterHook:function(test,context,{error, result, duration, passed, retries}, stepData,world) {
    // },
    /**
     * Function to be executed before a test (in Mocha/Jasmine) starts.
     */
    // beforeTest: function (test, context) {
    // },
    /**
     * Runs before a WebdriverIO command is executed.
     * @param {String} commandName hook command name
     * @param {Array} args arguments that the command would receive
     */
    // beforeCommand: function (commandName, args) {
    // },
    /**
     * Runs after a WebdriverIO command gets executed
     * @param {String} commandName hook command name
     * @param {Array} args arguments that command would receive
     * @param {Number} result 0 - command success, 1 - command error
     * @param {Object} error error object, if any
     */
    // afterCommand: function (commandName, args, result, error) {
    // },
    /**
     * Function to be executed after a test (in Mocha/Jasmine)
     */
    // afterTest: function (test, context, {error, result, duration, passed, retries}) {
    // },
    /**
     * Hook that gets executed after the suite has ended.
     * @param {Object} suite suite details
     */
    // afterSuite: function (suite) {
    // },
    /**
     * Gets executed after all tests are done. You still have access to all global variables from
     * the test.
     * @param {Number} result 0 - test pass, 1 - test fail
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that ran
     */
    // after: function (result, capabilities, specs) {
    // },
    /**
     * Gets executed right after terminating the webdriver session.
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {Array.<String>} specs List of spec file paths that ran
     */
    // afterSession: function (config, capabilities, specs) {
    // },
    /**
     * Gets executed after all workers have shut down and the process is about to exit.
     * An error thrown in the `onComplete` hook will result in the test run failing.
     * @param {Object} exitCode 0 - success, 1 - fail
     * @param {Object} config wdio configuration object
     * @param {Array.<Object>} capabilities list of capabilities details
     * @param {<Object>} results object containing test results
     */
    onComplete: function (exitCode, config, capabilities, results, browser, done) {
        // generate({
        //     // Required
        //     // This part needs to be the same path where you store the JSON files
        //     // default = '.tmp/json/'
        //     jsonDir: 'report/jsonFile',
        //     reportPath: 'report/report/',
        //     // for more options see https://github.com/wswebcreation/multiple-cucumber-html-reporter#options
        //   });

        // (async () => {
        //     await global.reportAggregator.createReport();
        // })();

        // const link = await RpService.getLaunchUrl(config);
        // console.log(`Report portal link ${link}`)
        // driver.takeScreenshot();
        // browser.takeScreenshot();
        // video.stop(browser, done)
        console.log('All done!');
    },
    /**
     * Gets executed when a refresh happens.
     * @param {String} oldSessionId session ID of the old session
     * @param {String} newSessionId session ID of the new session
     */
    // onReload: function (oldSessionId, newSessionId) {
    // },
    /**
     * Cucumber-specific hooks
     */
    beforeFeature: function (uri, feature, scenarios) {
        
    },
    beforeScenario: function (uri, feature, scenario, sourceLocation) {
        console.log(`Before ${JSON.stringify(scenario.pickle.name)}`);
    },
    beforeStep: function ({uri, feature, step}, context) {
    },
    afterStep: function ({uri, feature, step}, context , {error, result, duration, passed}) {
        // driver.takeScreenshot();
        // browser.takeScreenshot();
        // cucumberJson.attach(browser.takeScreenshot(), 'image/png');
        
        if (error) {
            myChromeBrowser.takeScreenshot();
            console.log('Steps failed');
           }

        //   if (!passed) {
        //     let failureObject = {};
        //     failureObject.type = 'afterStep';
        //     failureObject.error = error;
        //     failureObject.title = `${stepData.step.keyword}${stepData.step.text}`;
        //     const screenShot = global.browser.takeScreenshot();
        //     let attachment = Buffer.from(screenShot, 'base64');
        //     reportportal.sendFileToTest(failureObject, 'error', "screnshot.png", attachment);
        // }
    },
    afterScenario: function (uri, feature, scenario, result, sourceLocation) {
        // myChromeBrowser.reloadSession()
        // myChromeBrowser.pause(2000)
        // if (scenario.result.status === 'failed') {
        //     // browser.pause(10000)
        //     // const screenShot = await page.screenshot();
        //     // this.attach(screenShot,'image/png');
        //   };
        //   console.log(`After ${JSON.stringify(scenario.pickle.name)}`);
    },
    afterFeature: function (uri, feature, scenarios) {
    },

    reporters: [
        // [video, {
        //     saveAllVideos: true,       // If true, also saves videos for successful test cases
        //     videoSlowdownMultiplier: 20, // Higher to get slower videos, lower for faster videos [Value 1-100]
        //     videoRenderTimeout: 60, 
        //     usingAllure: true,
        //     jsonWireActions: [
        //         'url',
        //         'forward',
        //         'back',
        //         'refresh',
        //         'execute',
        //         'size',
        //         'position',
        //         'maximize',
        //         'click',
        //         'submit',
        //         'value',
        //         'keys',
        //         'clear',
        //         'selected',
        //         'enabled',
        //         'displayed',
        //         'orientation',
        //         'alert_text',
        //         'accept_alert',
        //         'dismiss_alert',
        //         'moveto',
        //         'buttondown',
        //         'buttonup',
        //         'doubleclick',
        //         'down',
        //         'up',
        //         'move',
        //         'scroll',
        //         'doubleclick',
        //         'longclick',
        //         'flick',
        //         'location',
        //         'sessions',
        //         'orientation',
        //         'status',
        //         'session',
        //         'sessionId',
        //         'timeouts',
        //         'async_script',
        //         'implicit_wait',
        //         'window_handle',
        //         'window_handles',
        //         'execute_async',
        //         'screenshot',
        //         'available_engines',
        //         'active_engine',
        //         'activated',
        //         'deactivate',
        //         'frame',
        //         'parent',
        //         'window',
        //         'cookie',
        //         'source',
        //         'title',
        //         'element',
        //         'elements',
        //         'keys',
        //         'other',
        //         'location',
        //         'location_in_view',
        //         'local_storage',
        //         'session_storage',
        //         'log',
        //         'types',
        //         'application_cache/status'

        //       ],
        //     //   recordAllActions: true,
        // }],
        'spec',
        ['allure', {
            outputDir: './report/allure-report/allure-result/',
            disableWebdriverStepsReporting: true,
            disableWebdriverScreenshotsReporting: false,
            useCucumberStepReporter: true,
          }],

        // ['sumologic', {
        //     // define sync interval how often logs get pushed to Sumologic
        //     syncInterval: 100,
        //     // endpoint of collector source
        //     sourceAddress: process.env.SUMO_SOURCE_ADDRESS
        // }],
        
        // 'dot',
        // ['json',{
        //     outputDir: './Results'
        // }]

        [ 'cucumberjs-json', {
            jsonFolder: './report/jsonFile',
            language: 'en',
        },
        ],
        // [reportportal, conf]
        // [HtmlReporter, {
        //     debug: true,
        //     outputDir: './reports/html-reports/',
        //     filename: 'report.html',
        //     reportTitle: 'Test Report Title',
            
        //     //to show the report in a browser when done
        //     showInBrowser: true,
 
        //     //to turn on screenshots after every test
        //     useOnAfterCommandForScreenshot: false,
 
        //     // to use the template override option, can point to your own file in the test project:
        //     // templateFilename: path.resolve(__dirname, '../template/wdio-html-reporter-alt-template.hbs'),
            
        //     // to add custom template functions for your custom template:
        //     // templateFuncs: {
        //     //     addOne: (v) => {
        //     //         return v+1;
        //     //     },
        //     // },
 
        //     //to initialize the logger
        //     LOG: log4js.getLogger("default")
        // }
        // ]

        // [
        //     'allure',
        //     {
        //         outputDir: './e2e/allure-report/allure-result/',
        //         disableWebdriverStepsReporting: false,
        //         disableWebdriverScreenshotsReporting: false,
        //         useCucumberStepReporter: true,
        //     },
        // ],
        
        // 'cucumberjs-json',

        // // OR like this if you want to set the folder and the language
        // [ 'cucumberjs-json', {
        //         jsonFolder: './test-report/cucumber-report',
        //         language: 'en',
        //     },
        // ],
    ],
};
