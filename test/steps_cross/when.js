import clearInputField from '../support_cross/action/clearInputField';
import clickElement from '../support_cross/action/clickElement';
import checkElementAndClickElement from '../support_cross/action/checkElementAndClickElement';
import closeLastOpenedWindow from '../support_cross/action/closeLastOpenedWindow';
import deleteCookies from '../support_cross/action/deleteCookies';
import dragElement from '../support_cross/action/dragElement';
import focusLastOpenedWindow from '../support_cross/action/focusLastOpenedWindow';
import handleModal from '../support_cross/action/handleModal';
import moveTo from '../support_cross/action/moveTo';
import pause from '../support_cross/action/pause';
import pressButton from '../support_cross/action/pressButton';
import scroll from '../support_cross/action/scroll';
import selectOption from '../support_cross/action/selectOption';
import selectOptionByIndex from '../support_cross/action/selectOptionByIndex';
import setCookie from '../support_cross/action/setCookie';
import setInputField from '../support_cross/action/setInputField';
import setInputFieldRandom from '../support_cross/action/setInputFieldRandom';
import setInputFieldRandomEmail from '../support_cross/action/setInputFieldRandomEmail';
import setPromptText from '../support_cross/action/setPromptText';
import refreshPage from '../support_cross/action/refreshBrowser';
import {holdDownKey, releaseKey} from '../support_cross/action/pressHoldAndRelease';



const { When } = require('@cucumber/cucumber');

When(
    /^I (click|doubleclick) on the (link|button|element) "([^"]*)?" by "([^"]*)?"$/,
    clickElement
);

When(
    /^I check element and (click|doubleclick) on the (link|button|element) "([^"]*)?" by "([^"]*)?"$/,
    checkElementAndClickElement
);

When(
    /^I (add|set) "([^"]*)?" to the inputfield "([^"]*)?" by "([^"]*)?"$/,
    setInputField
);

When(
    /^I (add|set) "([^"]*)?" random to the inputfield "([^"]*)?" by "([^"]*)?"$/,
    setInputFieldRandom
);

When(
    /^I (add|set) "([^"]*)?" random email to the inputfield "([^"]*)?" by "([^"]*)?"$/,
    setInputFieldRandomEmail
);

When(
    /^I clear the inputfield "([^"]*)?" by "([^"]*)?"$/,
    clearInputField
);

When(
    /^I drag element "([^"]*)?" to element "([^"]*)?" by "([^"]*)?"$/,
    dragElement
);

When(
    /^I pause for (\d+)ms by "([^"]*)?"$/,
    pause
);

When(
    /^I set a cookie "([^"]*)?" with the content "([^"]*)?" by "([^"]*)?"$/,
    setCookie
);

When(
    /^I delete the cookie "([^"]*)?" by "([^"]*)?"$/,
    deleteCookies
);

When(
    /^I refresh the page by "([^"]*)?"$/,
    refreshPage
);

When(
    /^I press "([^"]*)?" by "([^"]*)?"$/,
    pressButton
);

When(
    /^I press down "([^"]*)?" by "([^"]*)?"$/,
    holdDownKey
);

When(
    /^I release "([^"]*)?" by "([^"]*)?"$/,
    releaseKey
);

When(
    /^I (accept|dismiss) the (alertbox|confirmbox|prompt) by "([^"]*)?"$/,
    handleModal
);

When(
    /^I enter "([^"]*)?" into the prompt by "([^"]*)?"$/,
    setPromptText
);

When(
    /^I scroll to element "([^"]*)?" by "([^"]*)?"$/,
    scroll
);

When(
    /^I close the last opened (window|tab) by "([^"]*)?"$/,
    closeLastOpenedWindow
);

When(
    /^I focus the last opened (window|tab) by "([^"]*)?"$/,
    focusLastOpenedWindow
);

When(
    /^I select the (\d+)(st|nd|rd|th) option for element "([^"]*)?" by "([^"]*)?"$/,
    selectOptionByIndex
);

When(
    /^I select the option with the (name|value|text) "([^"]*)?" for element "([^"]*)?" by "([^"]*)?"$/,
    selectOption
);

When(
    /^I move to element "([^"]*)?"(?: with an offset of (\d+),(\d+))* by "([^"]*)?"$/,
    moveTo
);
