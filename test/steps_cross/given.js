import { Given } from '@cucumber/cucumber';

import checkContainsAnyText from '../support_cross/check/checkContainsAnyText';
import checkIsEmpty from '../support_cross/check/checkIsEmpty';
import checkContainsText from '../support_cross/check/checkContainsText';
import checkCookieContent from '../support_cross/check/checkCookieContent';
import checkCookieExists from '../support_cross/check/checkCookieExists';
import checkDimension from '../support_cross/check/checkDimension';
import checkElementExists from '../support_cross/check/checkElementExists';
import checkEqualsText from '../support_cross/check/checkEqualsText';
import checkModal from '../support_cross/check/checkModal';
import checkOffset from '../support_cross/check/checkOffset';
import checkProperty from '../support_cross/check/checkProperty';
import checkSelected from '../support_cross/check/checkSelected';
import checkTitle from '../support_cross/check/checkTitle';
import checkUrl from '../support_cross/check/checkURL';
import closeAllButFirstTab from '../support_cross/action/closeAllButFirstTab';
import compareText from '../support_cross/check/compareText';
import isEnabled from '../support_cross/check/isEnabled';
import isDisplayed from '../support_cross/check/isDisplayed';
import openWebsite from '../support_cross/action/openWebsite';
import setWindowSize from '../support_cross/action/setWindowSize';

Given(
    /^I open the (url|site) "([^"]*)?" by "([^"]*)?"$/,
    openWebsite
);

Given(
    /^the element "([^"]*)?" is( not)* displayed by "([^"]*)?"$/,
    isDisplayed
);

Given(
    /^the element "([^"]*)?" is( not)* enabled by "([^"]*)?"$/,
    isEnabled
);

Given(
    /^the element "([^"]*)?" is( not)* selected by "([^"]*)?"$/,
    checkSelected
);

Given(
    /^the checkbox "([^"]*)?" is( not)* checked by "([^"]*)?"$/,
    checkSelected
);

Given(
    /^there is (an|no) element "([^"]*)?" on the page by "([^"]*)?"$/,
    checkElementExists
);

Given(
    /^the title is( not)* "([^"]*)?" by "([^"]*)?"$/,
    checkTitle
);

Given(
    /^the element "([^"]*)?" contains( not)* the same text as element "([^"]*)?" by "([^"]*)?"$/,
    compareText
);

Given(
    /^the (button|element) "([^"]*)?"( not)* matches the text "([^"]*)?" by "([^"]*)?"$/,
    checkEqualsText
);

Given(
    /^the (button|element|container) "([^"]*)?"( not)* contains the text "([^"]*)?" by "([^"]*)?"$/,
    checkContainsText
);

Given(
    /^the (button|element) "([^"]*)?"( not)* contains any text by "([^"]*)?"$/,
    checkContainsAnyText
);

Given(
    /^the (button|element) "([^"]*)?" is( not)* empty by "([^"]*)?"$/,
    checkIsEmpty
);

Given(
    /^the page url is( not)* "([^"]*)?" by "([^"]*)?"$/,
    checkUrl
);

Given(
    /^the( css)* attribute "([^"]*)?" from element "([^"]*)?" is( not)* "([^"]*)?" by "([^"]*)?"$/,
    checkProperty
);

Given(
    /^the cookie "([^"]*)?" contains( not)* the value "([^"]*)?" by "([^"]*)?"$/,
    checkCookieContent
);

Given(
    /^the cookie "([^"]*)?" does( not)* exist by "([^"]*)?"$/,
    checkCookieExists
);

Given(
    /^the element "([^"]*)?" is( not)* ([\d]+)px (broad|tall) by "([^"]*)?"$/,
    checkDimension
);

Given(
    /^the element "([^"]*)?" is( not)* positioned at ([\d]+)px on the (x|y) axis by "([^"]*)?"$/,
    checkOffset
);

Given(
    /^I have a screen that is ([\d]+) by ([\d]+) pixels by "([^"]*)?"$/,
    setWindowSize
);

Given(
    /^I have osed all but the first (window|tab) by "([^"]*)?"$/,
    closeAllButFirstTab
);

Given(
    /^a (alertbox|confirmbox|prompt) is( not)* opened by "([^"]*)?"$/,
    checkModal
);
