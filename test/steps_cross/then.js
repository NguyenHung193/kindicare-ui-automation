import checkClass from '../support_cross/check/checkClass';
import checkContainsAnyText from '../support_cross/check/checkContainsAnyText';
import checkIsEmpty from '../support_cross/check/checkIsEmpty';
import checkContainsText from '../support_cross/check/checkContainsText';
import checkCookieContent from '../support_cross/check/checkCookieContent';
import checkCookieExists from '../support_cross/check/checkCookieExists';
import checkDimension from '../support_cross/check/checkDimension';
import checkEqualsText from '../support_cross/check/checkEqualsText';
import checkFocus from '../support_cross/check/checkFocus';
import checkInURLPath from '../support_cross/check/checkInURLPath';
import checkIsOpenedInNewWindow from
    '../support_cross/check/checkIsOpenedInNewWindow';
import checkModal from '../support_cross/check/checkModal';
import checkModalText from '../support_cross/check/checkModalText';
import checkNewWindow from '../support_cross/check/checkNewWindow';
import checkOffset from '../support_cross/check/checkOffset';
import checkProperty from '../support_cross/check/checkProperty';
import checkFontProperty from '../support_cross/check/checkFontProperty';
import checkSelected from '../support_cross/check/checkSelected';
import checkTitle from '../support_cross/check/checkTitle';
import checkTitleContains from '../support_cross/check/checkTitleContains';
import checkURL from '../support_cross/check/checkURL';
import checkURLPath from '../support_cross/check/checkURLPath';
import checkWithinViewport from '../support_cross/check/checkWithinViewport';
import compareText from '../support_cross/check/compareText';
import isEnabled from '../support_cross/check/isEnabled';
import isExisting from '../support_cross/check/isExisting';
import isVisible from '../support_cross/check/isDisplayed';
import isVisibleAndDeleteButton from '../support_cross/check/isDisplayedAndDeleteButton'
import waitFor from '../support_cross/action/waitFor';
import waitForVisible from '../support_cross/action/waitForDisplayed';
import checkIfElementExists from '../support_cross/lib/checkIfElementExists';

const { Then } = require('@cucumber/cucumber');

Then(
    /^I expect that the title is( not)* "([^"]*)?" by "([^"]*)?"$/,
    checkTitle
);

Then(
    /^I expect that the title( not)* contains "([^"]*)?" by "([^"]*)?"$/,
    checkTitleContains
);

Then(
    /^I expect that element "([^"]*)?" does( not)* appear exactly "([^"]*)?" times by "([^"]*)?"$/,
    checkIfElementExists
);

Then(
    /^I expect that element "([^"]*)?" is( not)* displayed by "([^"]*)?"$/,
    isVisible
);

Then(
    /^I expect that element "([^"]*)?" is( not)* displayed and delete by "([^"]*)?"$/,
    isVisibleAndDeleteButton
);

Then(
    /^I expect that element "([^"]*)?" becomes( not)* displayed by "([^"]*)?"$/,
    waitForVisible
);

Then(
    /^I expect that element "([^"]*)?" is( not)* within the viewport by "([^"]*)?"$/,
    checkWithinViewport
);

Then(
    /^I expect that element "([^"]*)?" does( not)* exist by "([^"]*)?"$/,
    isExisting
);

Then(
    /^I expect that element "([^"]*)?"( not)* contains the same text as element "([^"]*)?" by "([^"]*)?"$/,
    compareText
);

Then(
    /^I expect that (button|element) "([^"]*)?"( not)* matches the text "([^"]*)?" by "([^"]*)?"$/,
    checkEqualsText
);

Then(
    /^I expect that (button|element|container) "([^"]*)?"( not)* contains the text "([^"]*)?" by "([^"]*)?"$/,
    checkContainsText
);

Then(
    /^I expect that (button|element) "([^"]*)?"( not)* contains any text by "([^"]*)?"$/,
    checkContainsAnyText
);

Then(
    /^I expect that (button|element) "([^"]*)?" is( not)* empty by "([^"]*)?"$/,
    checkIsEmpty
);

Then(
    /^I expect that the url is( not)* "([^"]*)?" by "([^"]*)?"$/,
    checkURL
);

Then(
    /^I expect that the path is( not)* "([^"]*)?" by "([^"]*)?"$/,
    checkURLPath
);

Then(
    /^I expect the url to( not)* contain "([^"]*)?" by "([^"]*)?"$/,
    checkInURLPath
);

Then(
    /^I expect that the( css)* attribute "([^"]*)?" from element "([^"]*)?" is( not)* "([^"]*)?" by "([^"]*)?"$/,
    checkProperty
);

Then(
    /^I expect that the font( css)* attribute "([^"]*)?" from element "([^"]*)?" is( not)* "([^"]*)?" by "([^"]*)?"$/,
    checkFontProperty
);

Then(
    /^I expect that checkbox "([^"]*)?" is( not)* checked by "([^"]*)?"$/,
    checkSelected
);

Then(
    /^I expect that element "([^"]*)?" is( not)* selected by "([^"]*)?"$/,
    checkSelected
);

Then(
    /^I expect that element "([^"]*)?" is( not)* enabled by "([^"]*)?"$/,
    isEnabled
);

Then(
    /^I expect that cookie "([^"]*)?"( not)* contains "([^"]*)?" by "([^"]*)?"$/,
    checkCookieContent
);

Then(
    /^I expect that cookie "([^"]*)?"( not)* exists by "([^"]*)?"$/,
    checkCookieExists
);

Then(
    /^I expect that element "([^"]*)?" is( not)* ([\d]+)px (broad|tall) by "([^"]*)?"$/,
    checkDimension
);

Then(
    /^I expect that element "([^"]*)?" is( not)* positioned at ([\d+.?\d*]+)px on the (x|y) axis by "([^"]*)?"$/,
    checkOffset
);

Then(
    /^I expect that element "([^"]*)?" (has|does not have) the class "([^"]*)?" by "([^"]*)?"$/,
    checkClass
);

Then(
    /^I expect a new (window|tab) has( not)* been opened by "([^"]*)?"$/,
    checkNewWindow
);

Then(
    /^I expect the url "([^"]*)?" is opened in a new (tab|window) by "([^"]*)?"$/,
    checkIsOpenedInNewWindow
);

Then(
    /^I expect that element "([^"]*)?" is( not)* focused by "([^"]*)?"$/,
    checkFocus
);

Then(
    /^I wait on element "([^"]*)?"(?: for (\d+)ms)*(?: to( not)* (be checked|be enabled|be selected|be displayed|contain a text|contain a value|exist|be clickable))* by "([^"]*)?"$/,
    {
        wrapperOptions: {
            retry: 3,
        },
    },
    waitFor
);

Then(
    /^I expect that a (alertbox|confirmbox|prompt) is( not)* opened by "([^"]*)?"$/,
    checkModal
);

Then(
    /^I expect that a (alertbox|confirmbox|prompt)( not)* contains the text "([^"]*)?" by "([^"]*)?"$/,
    checkModalText
);
