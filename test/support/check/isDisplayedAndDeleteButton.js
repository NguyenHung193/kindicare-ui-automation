import checkIfElementExists from '../lib/checkIfElementExists';
import waitForElement from '../action/waitFor';
import clickBtn from '../action/clickElement'
import sleep from '../action/pause'
import refresh from '../action/refreshBrowser'
import {loginPage , random, homePage, loginAccionaPage, homeAccionaPage, myTimeSheetAccionaPage, plantInspectionAccionaPage, sideMenuAccionaPage, leaveRequestAccionaPage, eventsAccionaPage} from '../../pageObjects/exportPage'

/**
 * Check if the given element is (not) visible
 * @param  {String}   selector   Element selector
 * @param  {String}   falseCase Check for a visible or a hidden element
 */
export default (selector,falseCase) => {
    const selector2 = (((loginPage)[selector])||((homePage)[selector])||((loginAccionaPage)[selector])||((homeAccionaPage)[selector])||((myTimeSheetAccionaPage)[selector])||((plantInspectionAccionaPage)[selector])||((sideMenuAccionaPage)[selector])||((leaveRequestAccionaPage)[selector])||((eventsAccionaPage)[selector]));
    /**
     * Visible state of the give element
     * @type {String}
     */
    // const isDisplayed = $(selector2).isDisplayed();

    // if (falseCase) {
    //     expect(isDisplayed).not.toEqual(
    //         true,
    //         `Expected element "${selector2}" not to be displayed`
    //     );
    // } else {
    //     expect(isDisplayed).toEqual(
    //         true,
    //         `Expected element "${selector2}" to be displayed`
    //     );
    // }
    const deleteFunction = () => {
        // sleep(5000);
        waitForElement('summaryBtn', '20000', null , 'be displayed');
        clickBtn('click','element','summaryBtn')
        waitForElement('loadingSummaryIcon', '20000', 'not', 'be displayed');
        // sleep(2000);
        const isDisplayed = $(selector2).isExisting();
        console.log("-------------------True", isDisplayed);
        
        if (isDisplayed === true) {
            console.log("DeleteFunction++++++++++++++++++++");
            sleep(2000);
            waitForElement('deleteBtn', '20000', null , 'be clickable');
            clickBtn('click','element','deleteBtn')
            sleep(2000);
            waitForElement('deleteConfirmBtn', '20000', null , 'be clickable');
            sleep(2000);
            clickBtn('click','element','deleteConfirmBtn')
            // waitForElement('deleteNewExternalPlantSuccessMessage', '20000', null , 'be displayed');
            sleep(2000);
            refresh();
            sleep(2000);
            return deleteFunction();
        } else {
            return;
        }
    }

    deleteFunction();
};
