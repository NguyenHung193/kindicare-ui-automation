/**
* main page object containing all methods, selectors and functionality
* that is shared across all page objects
*/
import { assert, expect } from 'chai';

module.exports = class Page {
    /**
    * Opens a sub page of the page
    * @param path path of the sub page (e.g. /path/to/page.html)
    */
    // open (path) {
    //     return browser.url(`https://the-internet.herokuapp.com/${path}`)
    // }
    constructor(){

    }

    clickToElement(element) {
        $(element).isDisplayed()
        browser.pause(1000);
        $(element).click();
        // browser.pause(3000);
    }

    sendsKeyToElement(element,value){
        $(element).isDisplayed()
        browser.pause(1000);
        $(element).setValue(value);
    }

    elementIsDisplayed (element){
        var ab =  $(element).isDisplayed()
        browser.pause(1000);
        expect(ab).to.equal(true);
    }

    elementIsUndisplayed(element){
        var ab =  $(element).isDisplayed()
        browser.pause(1000);
        expect(ab).to.equal(false);
    }
}
