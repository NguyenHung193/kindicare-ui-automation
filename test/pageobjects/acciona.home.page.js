const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
    
    constructor() { 
        super();
        this.homeIcon = '//android.widget.TextView[@text="Home"]';
        this.plantIcon = '//android.widget.TextView[@text="Plant"]';
        this.eventsIcon = '//android.widget.TextView[@text="Events"]';
        this.timesheetIcon = '//android.widget.TextView[@text="Timesheet"]';
        this.leaveIcon = '//android.widget.TextView[@text="Leave"]';
        this.menuIcon = '//android.widget.TextView[@content-desc="left_menu"]';
        // this.timesheetIcon = '//android.widget.ImageView[@text="Timesheet"]';
        // this.timesheetIcon = '//android.widget.ImageView[@text="Timesheet"]';
    }
}

module.exports = new LoginPage();
