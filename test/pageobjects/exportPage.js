const loginPage = require('../pageObjects/login.page');
const homePage = require('../pageObjects/home.page');
const loginAccionaPage = require('../pageObjects/acciona.login.page');
const homeAccionaPage = require('../pageObjects/acciona.home.page');
const myTimeSheetAccionaPage = require('../pageObjects/acciona.myTimeSheet.page');
const plantInspectionAccionaPage = require('../pageObjects/acciona.plantInspection.page');
const sideMenuAccionaPage = require('../pageObjects/acciona.sideMenu.page');
const leaveRequestAccionaPage = require('../pageObjects/acciona.leaveRequest.page');
const eventsAccionaPage = require('../pageObjects/acciona.events.page');

const random = require('../support_cross/lib/random');


export {random};
export {loginPage};
export {homePage};
export {loginAccionaPage};
export {homeAccionaPage};
export {myTimeSheetAccionaPage};
export {plantInspectionAccionaPage};
export {sideMenuAccionaPage};
export {leaveRequestAccionaPage};
export {eventsAccionaPage};