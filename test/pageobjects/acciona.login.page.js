const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class LoginPage extends Page {
    
    constructor() { 
        super();
        this.loginBtn = '//android.widget.TextView[@text="Login"]|//android.view.ViewGroup[@content-desc="Login"]/android.widget.TextView';
        this.emailInput = '//android.widget.EditText[@resource-id="signInFormUsername"]';
        this.passwordInput = '//android.widget.EditText[@resource-id="signInFormPassword"]';
        this.signInBtn = '//android.widget.Button[@text="submit"]';
        
    }
}

module.exports = new LoginPage();
