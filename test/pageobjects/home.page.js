const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class HomePage extends Page {
    
    constructor() { 
        super();
        // //kindiCare homepage browser
        // this.dashboardUserInfo = "//span[@class='userInfo']"
        // this.centresWeb = "//span[contains(text(),'Centres')]";
        // this.createCentresWeb = "//span[contains(text(),'Create')]";
        // this.inputNameCentresCreateWeb = "//input[@id='name']";
        // this.inputBusinessCentresCreateWeb = "//input[@id='businessName']";
        // this.inputEnquiryEmailCentresCreateWeb = "//input[@id='enquiryEmail']";
        // this.adminEmailCentresCreateWeb = "//input[@id='adminEmail']";
        // this.emailCentresCreateWeb = "//input[@id='email']";
        // this.countryCentresCreateWeb = "//input[@id='country']";
        // this.cityCentresCreateWeb = "//input[@id='city']";
        // this.contentTypeCentresCreateWeb = "//input[@id='type']";
        // this.stateCentresCreateWeb = "//div[@class='content-form']//div[@placeholder='State']//input[@id='state' and @role= 'combobox']";
        // this.postCodeCentresCreateWeb = "//input[@id='postCode']";
        // this.addressCentresCreateWeb = "//input[@id='addressL1']"
        // this.locationCentresCreatWeb = "//input[@type='search' and contains(@id,'rc_select_') and not(@unselectable)]";
        // this.createButtonCentresCreateWeb = "//button[@type='submit']//span[contains(text(),'Create')]";
        // this.successMessage = "//div[contains(text(),'The update is successful!')]";

        // //kindicare homepage app
        // this.searchInput = '//android.widget.TextView[@text="Search location or childcare"]';
        // this.currentDate = new Date();
    }

    entryDate(date,month) {
        return this.dateEntries = '//android.view.View[@content-desc="' + date + ' ' + month + ' 2020"]';
    }

    entryCurrentDate() {
        return this.dateEntries = '//android.view.View[@content-desc="' + ("0" + this.currentDate.getDate()).slice(-2) + ' ' + this.monthNames[this.currentDate.getMonth()] + ' 2020"]';
    }
    // /**
    //  * define selectors using getter methods
    //  */
    // get inputUsername () { return $('#username') }
    // get inputPassword () { return $('#password') }
    // get btnSubmit () { return $('button[type="submit"]') }

    // /**
    //  * a method to encapsule automation code to interact with the page
    //  * e.g. to login using username and password
    //  */
    // login (username, password) {
    //     this.inputUsername.setValue(username);
    //     this.inputPassword.setValue(password);
    //     this.btnSubmit.click(); 
    // }

    // /**
    //  * overwrite specifc options to adapt it to page object
    //  */
    // open () {
    //     return super.open('login');
    // }
}

module.exports = new HomePage();
