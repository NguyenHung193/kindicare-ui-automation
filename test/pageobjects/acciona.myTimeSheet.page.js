const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class MyTimeSheet extends Page {
    
    constructor() { 
        super();

        this.addNewEntryBtn = '//android.widget.TextView[@text=Add New Entry"]';
        this.timesheetEntry = '//android.widget.TextView[@text=Timesheet Entry"]';
        this.timesheetSummary = '//android.widget.TextView[@text=Timesheet Summary"]';
        this.expandIcon = '//android.view.ViewGroup[@content-desc="expandIcon"]';
        this.editTimeSheetBtn = '//android.widget.TextView[@text="Edit"]';
        this.deleteTimeSheetBtn = '//android.widget.TextView[@text="Delete"]';

        //add new entry
        this.selectJob = '//android.widget.TextView[@text="Select Job"]';
        this.selectCostCode = '//android.widget.TextView[@text="Select cost code"]';
        this.selectLocationId = '//android.widget.TextView[@text="Location ID*"]';
        this.startTime = '//android.widget.TextView[@text="Start Time"]//following-sibling::android.view.ViewGroup';
        this.endTime = '////android.widget.TextView[@text="End Time"]//following-sibling::android.view.ViewGroup';
        this.leaveType = '//android.widget.TextView[@text="Leave type"]';
        this.timeAllowance = '//android.widget.TextView[@text="Time allowance"]';
        this.LAHA = '//android.widget.TextView[@text="LAHA"]';
        this.description = '//android.widget.TextView[@text="Description"]';
        this.payrollNotes = '//android.widget.TextView[@text="Payroll Notes"]';
        this.doneIcon = "//android.widget.TextView[@content-desc='DoneIcon']";
        this.backIcon = "//android.widget.TextView[@content-desc='backIcon']";
        this.homeIcon = "//android.widget.TextView[@content-desc='homeIcon']";
        this.imSureConfirmation = "//android.widget.TextView[@text='I'M SURE']";
        this.closeConfirmation = "//android.widget.TextView[@text='CLOSE']";
        this.yesConfirmation = "//android.widget.TextView[@text='YES']";
        this.cancelConfirmation = "//android.widget.TextView[@text='CANCEL']";
    }
}

module.exports = new MyTimeSheet();
