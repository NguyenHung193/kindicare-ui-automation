const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class PlantInspection extends Page {
    
    constructor() { 
        super();

        //existing Plant
        this.existingPlant = '//android.widget.TextView[@text="Existing Plant"]';
        this.plantInspectionSelect = '//android.widget.TextView[@text="Please select a plant"]';
        this.pleaseSelectAPlant = '//android.widget.EditText[@text="Please select a plant"]';
        this.firstPlant = '(//android.widget.EditText[@text="Please select a plant"]/following-sibling::android.widget.ScrollView//android.view.ViewGroup)[1]';
        this.newPlantInspectionBtn = '//android.widget.TextView[@text="New Inspection"]';
        

        //new Inspection
        this.selectAJobNewInspection= '//android.widget.TextView[@text="Select a job"]';
        this.selectOperator= '//android.widget.TextView[@text="Select Operator"]';

        this.numericEntry= '//android.widget.EditText[@text="Numeric Entry"]';
        this.addNotes= '//android.widget.EditText[@text="Add Notes"]';
        this.addPhoto= '//android.widget.TextView[@text="Plant Photo *"]/following-sibling::android.view.ViewGroup';
        this.jobRequired = '//android.widget.TextView[@text="Job is required"]';
        this.operatorRequired = '//android.widget.TextView[@text="Operator is required"]';
        this.readingRequired = '//android.widget.TextView[@text="Reading is required"]';
        this.descriptionRequired = '//android.widget.TextView[@text="Description is required"]';
        this.imagesRequired = '//android.widget.TextView[@text="At least one image is required"]';
        this.selectImagesPopup = '//android.widget.TextView[@text="Select Images"]';	
        this.selectCamera = '//android.widget.Button[@text="CAMERA"]';
        this.takeCamera = '//android.widget.TextView[@text="Take"]';

        //select a job
        this.firstJob = '(//android.widget.EditText[@text="Select a job"]/following-sibling::android.widget.ScrollView//android.view.ViewGroup)[1]';

        //select operator
        this.firstOperator = '(//android.widget.EditText[@text="Select Operator"]/following-sibling::android.widget.ScrollView//android.view.ViewGroup)[1]';
        
        //allow camera
        this.allowCamera = '//android.widget.Button[@text="While using the app"]';

        //new plant
        this.newPlant = '//android.widget.TextView[@text="New Plant"]';
        this.newPlantInspectionInput = '//android.widget.TextView[@text="New Inspection"]';
    }
}

module.exports = new PlantInspection();
