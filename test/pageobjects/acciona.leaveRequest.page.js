const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class SideMenu extends Page {
    
    constructor() { 
        super();
        this.newLeaveRequestBtn = '//android.widget.TextView[@text="New Leave Request"]';
        this.nextStepBtn = '//android.widget.TextView[@text="Next Step"]';
        this.selectLeaveType = '//android.widget.TextView[@text="Select Leave Type"]';
        this.enterReasonForLeave = '//android.widget.TextView[@text="Enter reason for leave"]';
        this.startDate = '//android.widget.TextView[@text="Start Date/Time *"]//following-sibling::android.view.ViewGroup';
        this.endDate = '//android.widget.TextView[@text="End Date/Time *"]//following-sibling::android.view.ViewGroup';
        this.selectLeaveType = '//android.widget.TextView[@text="Submit"]';
        this.backIcon = "//android.widget.TextView[@content-desc='backIcon']";
        
    }
}

module.exports = new SideMenu();
