const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class Events extends Page {
    
    constructor() { 
        super();

        this.addEventIcon = '//android.widget.TextView[@content-desc="addEvent"]';
        
    }
}

module.exports = new Events();
