const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */
class SideMenu extends Page {
    
    constructor() { 
        super();
        this.logoSideMenu = '(//android.view.ViewGroup[@content-desc="Menu-Side-Logo"])[1]';
        this.homeSideMenu = '(//android.view.ViewGroup[@content-desc="Home-side-menu"])[1]';
        this.myProfileSideMenu = '(//android.view.ViewGroup[@content-desc="MyProfile-side-menu"])[1]';
        this.myRequisitionsSideMenu = '(//android.view.ViewGroup[@content-desc="MyRequisitions-side-menu"])[1]';
        this.linkSideMenu = '(//android.view.ViewGroup[@content-desc="Links-side-menu"])[1]';
        this.contactUsSideMenu = '(//android.view.ViewGroup[@content-desc="ContactUs-side-menu"])[1]';
        this.logoutSideLogo = '//android.widget.TextView[@content-desc="Logout-side-menu"]';
        this.menuSideLogo = '//android.widget.ImageView[@text="Home"]';
        
    }
}

module.exports = new SideMenu();
