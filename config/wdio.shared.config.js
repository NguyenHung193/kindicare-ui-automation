// const shell = require('shelljs')
// const video = require('wdio-video-reporter')
// var cp = require('child_process')
// var sleep = require('sleep')
// const ReportAggregator = require('@rpii/wdio-html-reporter').ReportAggregator
// const HtmlReporter = require('@rpii/wdio-html-reporter').HtmlReporter
// const log4js = require('@log4js-node/log4js-api')
// var rimraf = require('rimraf')
// var fs = require('fs')
// const { generate } = require('multiple-cucumber-html-reporter')
// const { removeSync } = require('fs-extra')

// exports.config = {
//   // ====================
//   // Runner and framework
//   // Configuration
//   // ====================
//   host: 'localhost',
//   path: '/wd/hub/',
//   mochaOpts: {
//     ui: 'bdd',
//     timeout: 60000,
//     require: ['@babel/register']
//   },
//   capabilities: [
//     {
//       // "platformName": "Android",
//       // "automationName": "UiAutomator2",
//       // "uiid": "emulator-5554",
//       // "appPackage": "com.linkedsite.mobile",
//       // "appActivity": ".MainActivity",
//       // browserName: "S9",
//       // "browserName": "S9",
//       // "deviceName": "Samsung",
//       // "metadata": {
//       //     device: 'Samsung Galaxy S7 Edge',
//       //     platform: {
//       //     name: 'android',
//       //     version: '8.0'
//       //     }
//       //     }
//       // 'newCommandTimeout': "2000",
//       // "newCommandTimeout": 60000000,
//     }
//   ],
//   runner: 'local',
//   framework: 'cucumber',
//   sync: true,
//   logLevel: 'debug',
//   deprecationWarnings: true,
//   outputDir: './test-report/output',
//   app: '',
//   bail: 0,
//   // baseUrl: 'http://the-internet.herokuapp.com',
//   waitforTimeout: 6000,
//   connectionRetryTimeout: 90000,
//   connectionRetryCount: 3,

//   reporters: [
//     [
//       video,
//       {
//         saveAllVideos: true, // If true, also saves videos for successful test cases
//         videoSlowdownMultiplier: 20 // Higher to get slower videos, lower for faster videos [Value 1-100]
//       }
//     ],
//     'spec',
//     [
//       'allure',
//       {
//         outputDir: './e2e-test/allure-report/allure-result/',
//         disableWebdriverStepsReporting: false,
//         disableWebdriverScreenshotsReporting: false,
//         useCucumberStepReporter: true
//       }
//     ]

//     // 'cucumberjs-json',

//     // // OR like this if you want to set the folder and the language
//     // [ 'cucumberjs-json', {
//     //         jsonFolder: './test-report/cucumber-report',
//     //         language: 'en',
//     //     },
//     // ],
//   ],
//   cucumberOpts: {
//     requireModule: ['@babel/register'],
//     backtrace: true,
//     compiler: [],
//     dryRun: false,
//     failFast: false,
//     format: ['pretty'],
//     colors: true,
//     snippets: true,
//     source: true,
//     profile: [],
//     strict: false,
//     tags: [],
//     timeout: 100000,
//     ignoreUndefinedDefinitions: false,
//     tagExpression: 'not @skip'
//     // require: ['test/Steps/login'],
//   },

//   // ====================
//   // Appium Configuration
//   // ====================
//   services: [
//     [
//       'appium',
//       {
//         agrs: {
//           // For options see
//           // https://github.com/webdriverio/webdriverio/tree/master/packages/wdio-appium-service
//           // If you need a logs from appium server, make log equal true.
//           log: true,
//           args: {
//             // platformName: "Android",
//             // automationName: "UiAutomator2",
//             // uiid: "emulator-5554",
//             // appPackage: "com.linkedsite.mobile",
//             // appActivity: ".MainActivity",
//             // port: 4723
//             // For arguments see
//             // https://github.com/webdriverio/webdriverio/tree/master/packages/wdio-appium-service
//           },
//           command: 'appium'
//         }
//       }
//     ]
//   ],

//   port: 4723,

//   // ====================
//   // Some hooks
//   // ====================

//   //This code is responsible for taking the screenshot in case of error and attaching it to the report
//   afterStep(uri, feature, scenario) {
//     if (scenario.error) {
//       driver.takeScreenshot()
//     }
//   },
//   //  afterTest: function (test) {
//   //     const path = require('path');
//   //     const moment = require('moment');

//   //     // if test passed, ignore, else take and save screenshot.
//   //     if (test.passed) {
//   //         return;
//   //     }
//   //     const timestamp = moment().format('YYYYMMDD-HHmmss.SSS');
//   //     const filepath = path.join('reports/html-reports/screenshots/', timestamp + '.png');
//   //     browser.saveScreenshot(filepath);
//   //     process.emit('test:screenshot', filepath);
//   // },
//   afterStep: function (test, context, { error, result, duration, passed, retries }) {
//     if (error) {
//       driver.takeScreenshot()
//     }
//   },
//   onPrepare: function () {
//     // cp.exec('appium --port 4723',
//     // (error, stdout, stderr) => {
//     //     console.log(stdout);
//     //     console.log(stderr);
//     //     if (error !== null) {
//     //         console.log(`exec error: ${error}`);
//     //     }
//     // });
//     // removeSync('allure-report');
//     cp.exec('node e2e-test/deleteReport.js', (error, stdout, stderr) => {
//       console.log(stdout)
//       console.log(stderr)
//       if (error !== null) {
//         console.log(`exec error: ${error}`)
//       }
//     })
//     // shell.exec('node deleteReport.js')
//     // sleep.sleep(5)
//     // var dir = 'test-report';

//     // if (!fs.existsSync(dir)){
//     //     fs.mkdirSync(dir);
//     // }
//     var workDir = process.cwd()
//     cp.exec('pwd', (error, stdout, stderr) => {
//       console.log(stdout)
//       console.log(stderr)
//       if (error !== null) {
//         console.log(`exec error: ${error}`)
//       }
//     })
//     cp.exec(`cd ${workDir}/e2e-test && sh startEmulator.sh`, (error, stdout, stderr) => {
//       console.log(stdout)
//       console.log(stderr)
//       if (error !== null) {
//         console.log(`exec error: ${error}`)
//       }
//     })
//     sleep.sleep(30)
//     shell.exec(
//       'adb -s emulator-5556 install /Users/hungnguyen/Desktop/linked-site-mobile/android/app/build/outputs/apk/releaseStaging/app-releaseStaging.apk'
//     )
//     console.log('Start testing.....')
//     sleep.sleep(10)
//     browser.reset()
//     sleep.sleep(10)
//     shell.exec('adb start-server')

//     // let reportAggregator = new ReportAggregator({
//     //     outputDir: './reports/html-reports/',
//     //     filename: 'master-report.html',
//     //     reportTitle: 'Master Report',
//     //     // browserName : browser.capabilities.browserName,
//     //     // to use the template override option, can point to your own file in the test project:
//     //     // templateFilename: path.resolve(__dirname, '../template/wdio-html-reporter-alt-template.hbs')
//     // });
//     // reportAggregator.clean() ;

//     // global.reportAggregator = reportAggregator;
//   },

//   onComplete: function () {
//     //     cp.exec('adb shell reboot -p',
//     // (error, stdout, stderr) => {
//     //     console.log(stdout);
//     //     console.log(stderr);
//     //     if (error !== null) {
//     //         console.log(`exec error: ${error}`);
//     //     }
//     // });
//     // cp.exec('adb kill-server',
//     // (error, stdout, stderr) => {
//     //     console.log(stdout);
//     //     console.log(stderr);
//     //     if (error !== null) {
//     //         console.log(`exec error: ${error}`);
//     //     }
//     // });

//     //    shell.exec('adb kill-server')
//     //    driver.deleteSession();
//     shell.exec('adb uninstall com.linkedsite.mobile')
//     shell.exec('adb shell reboot -p')
//     //    shell.exec('adb shell reboot -p')
//     shell.exec('adb kill-server')
//     shell.exec('killall node')

//     console.log('End testing...')

//     // (async () => {
//     //     await global.reportAggregator.createReport();
//     // })();
//   },

//   beforeScenario: function (uri, feature, scenario, sourceLocation, context) {
//     sleep.sleep(5)
//     browser.reset()
//     sleep.sleep(5)
//   },

//   afterFeature: function (uri, feature, scenarios) {},

//   beforeFeature: function (uri, feature, scenarios) {
//     // shell.exec('adb -s emulator-5554 install /Users/hungnguyen/Downloads/newSrc/android/app/build/outputs/apk/releaseStaging/app-releaseStaging.apk')
//     console.log('Installing.....')
//     // sleep.sleep(5)
//     // sleep.sleep(5)
//   },

//   specs: ['e2e-test/test/features/**/*.feature']
// }
