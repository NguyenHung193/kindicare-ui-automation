// const path = require('path');
// const { hooks } = require('../test/support/hooks');
// const RerunService = require('wdio-rerun-service');
// const AndroidInfo = require('./android.info');
// const shell = require("shelljs")
// const video = require('wdio-video-reporter');
// var cp = require('child_process');
// var sleep = require('sleep');

// // const reportportal = require('wdio-reportportal-reporter');
// // const RpService = require("wdio-reportportal-service");



// exports.config = {
//     host: 'localhost',
//     path: '/wd/hub/',
//     port: 4723,
//     mochaOpts: {
//         ui: 'bdd',
//         timeout: 60000,
//         require: ['@babel/register']
//     },
//     sync: true,
//     services: [
//         ['appium',{
//             command: 'appium',
//             relaxedSecurity: true,
//             logPath: "./" ,
//             log: true,
//             port: 4723,
//             args: {
//                 // For options see
//                 // https://github.com/webdriverio/webdriverio/tree/master/packages/wdio-appium-service
//                 // If you need a logs from appium server, make log equal true.
//                 // relaxedSecurity: true,
//                 // logPath: "./" ,
//                 // log: true,
//                 // args: 
//                     // port: 4723,
//                     // platformName: "Android",
//                     // automationName: "UiAutomator2",
//                     // uiid: "emulator-5554",
//                     // appPackage: "com.linkedsite.mobile",
//                     // appActivity: ".MainActivity",
//                     // port: 4723,
//                     // deviceName: "Bi2"
//                     // platformName: 'Android',
//                 newCommandTimeout: 240,
//                 appPackage: "com.linkedsite.mobile",
//                 noReset: false,
//                 fullReset: true,
//                 automationName: 'uiautomator2',
//                 uiid: "emulator-5554",
//                 deviceName: AndroidInfo.deviceName(),
//                 platformVersion: AndroidInfo.platFormVersion(),
//                     // For arguments see
//                     // https://github.com/webdriverio/webdriverio/tree/master/packages/wdio-appium-service
                
                
//             }
// }]],
//     // services: [
//     //     ['selenium-standalone', {
//     //         logPath: 'logs',
//     //         installArgs: { 
//     //             chrome: { version: 'latest' }, 
//     //             drivers : {
//     //                 chrome : {
//     //                     version : "latest",
//     //                 }
//     //             }
//     //     }, // drivers to install
//     //         args: { 
//     //             chrome: { version: 'latest' },
//     //             drivers : {
//     //                 chrome : {
//     //                     version : "latest",
//     //                 },
//     //             },
//     //             seleniumArgs: ['-host', '127.0.0.1','-port', '4444']
//     //         } // drivers to use
//     //     }],],
      
//     //
//     // ====================
//     // Runner Configuration
//     // ====================
//     //
//     // WebdriverIO allows it to run your tests in arbitrary locations (e.g. locally or
//     // on a remote machine).
//     runner: 'local',
//     sync: true,
//     deprecationWarnings: true,
//     // app: '',
//     //
//     // ==================
//     // Specify Test Files
//     // ==================
//     // Define which test specs should run. The pattern is relative to the directory
//     // from which `wdio` was called. Notice that, if you are calling `wdio` from an
//     // NPM script (see https://docs.npmjs.com/cli/run-script) then the current working
//     // directory is where your package.json resides, so `wdio` will be called from there.
//     //
//     specs: [
//         'test/features/**/*.feature'
//     ],
//     // Patterns to exclude.
//     exclude: [
//         // 'path/to/excluded/files'
//     ],
//     //
//     // ============
//     // Capabilities
//     // ============
//     // Define your capabilities here. WebdriverIO can run multiple capabilities at the same
//     // time. Depending on the number of capabilities, WebdriverIO launches several test
//     // sessions. Within your capabilities you can overwrite the spec and exclude options in
//     // order to group specific specs to a specific capability.
//     //
//     // First, you can define how many instances should be started at the same time. Let's
//     // say you have 3 different capabilities (Chrome, Firefox, and Safari) and you have
//     // set maxInstances to 1; wdio will spawn 3 processes. Therefore, if you have 10 spec
//     // files and you set maxInstances to 10, all spec files will get tested at the same time
//     // and 30 processes will get spawned. The property handles how many capabilities
//     // from the same test should run tests.
//     //
//     maxInstances: 1,
//     //
//     // If you have trouble getting all important capabilities together, check out the
//     // Sauce Labs platform configurator - a great tool to configure your capabilities:
//     // https://docs.saucelabs.com/reference/platforms-configurator
//     //
//     capabilities: {
//         // myChromeBrowser: {
//         //     capabilities: {
//         //         browserName: 'chrome',
//         //     'goog:chromeOptions': {
//         //         // to run chrome headless the following flags are required
//         //         // (see https://developers.google.com/web/updates/2017/04/headless-chrome)
//         //         args: ['--disable-gpu','--no-sandbox', '--window-size=1920,1080'],
//         //         },
//         //     }
//         // },
//         // myAppiumAppr: {
//         //     port: 4723,
//         //     capabilities: {
//         //         platformName: 'Android',
//         //         newCommandTimeout: 240,
//         //         appPackage: "com.linkedsite.mobile",
//         //         noReset: false,
//         //         fullReset: true,
//         //         automationName: 'uiautomator2',
//         //         uiid: "emulator-5554",
//         //         deviceName: AndroidInfo.deviceName(),
//         //         platformVersion: AndroidInfo.platFormVersion(),
//         //         // app: path.resolve(`./app/${AndroidInfo.appName()}`)
//         //             }
//         // }
//     },
//     //
//     // ===================
//     // Test Configurations
//     // ===================
//     // Define all options that are relevant for the WebdriverIO instance here
//     //
//     // Level of logging verbosity: trace | debug | info | warn | error | silent
//     logLevel: 'trace',
//     // outputDir: path.join(__dirname, '/logs'),
//     outputDir: './_results_',
//     //
//     // Set specific log levels per logger
//     // loggers:
//     // - webdriver, webdriverio
//     // - @wdio/applitools-service, @wdio/browserstack-service,
//     //   @wdio/devtools-service, @wdio/sauce-service
//     // - @wdio/mocha-framework, @wdio/jasmine-framework
//     // - @wdio/local-runner, @wdio/lambda-runner
//     // - @wdio/sumologic-reporter
//     // - @wdio/cli, @wdio/config, @wdio/sync, @wdio/utils
//     // Level of logging verbosity: trace | debug | info | warn | error | silent
//     // logLevels: {
//     //     webdriver: 'info',
//     //     '@wdio/applitools-service': 'info'
//     // },
//     //
//     // If you only want to run your tests until a specific amount of tests have failed use
//     // bail (default is 0 - don't bail, run all tests).
//     bail: 0,
//     //
//     // Set a base URL in order to shorten url command calls. If your `url` parameter starts
//     // with `/`, the base url gets prepended, not including the path portion of your baseUrl.
//     // If your `url` parameter starts without a scheme or `/` (like `some/path`), the base url
//     // gets prepended directly.
//     baseUrl: 
//     // 'https://linkedsite-uat.acciona.com.au',
//     'https://kindicare-super-admin-web-staging.enouvo.com',
//     //https://linkedsite-uat.acciona.com.au
//     // Default timeout for all waitFor* commands.
//     waitforTimeout: 60000,
//     //
//     // Default timeout in milliseconds for request
//     // if browser driver or grid doesn't send response
//     connectionRetryTimeout: 60000,
//     //
//     // Default request retries count
//     connectionRetryCount: 3,
//     //
//     // Test runner services
//     // Services take over a specific job you don't want to take care of. They enhance
//     // your test setup with almost no effort. Unlike plugins, they don't add new
//     // commands. Instead, they hook themselves up into the test process.
//     // services: [],
//     //
//     // Framework you want to run your specs with.
//     // The following are supported: Mocha, Jasmine, and Cucumber
//     // see also: https://webdriver.io/docs/frameworks.html
//     //
//     // Make sure you have the wdio adapter package for the specific framework installed
//     // before running any tests.
//     framework: 'cucumber',
//     //
//     // The number of times to retry the entire specfile when it fails as a whole
//     // specFileRetries: 1,
//     //
//     // Whether or not retried specfiles should be retried immediately or deferred
//     // to the end of the queue specFileRetriesDeferred: false,
//     //
//     // Test reporter for stdout.
//     // The only one supported by default is 'dot'
//     // see also: https://webdriver.io/docs/dot-reporter.html
//     // reporters: ['spec'],
//     //
//     // If you are using Cucumber you need to specify the location of your step definitions.
//     // dockerOptions: {
//     //     image: 'selenium/standalone-chrome',
//     //     healthCheck: 'http://localhost:4444',
//     //     options: {
//     //         p: ['4444:4444'],
//     //         shmSize: '2g'
//     //     }
//     // },
//     // dockerLogs: './',
    
//     cucumberOpts: {
//         // <boolean> show full backtrace for errors
//         backtrace: true,
//         // <string[]> module used for processing required features
//         requireModule: ['@babel/register'],
//         // <boolean< Treat ambiguous definitions as errors
//         failAmbiguousDefinitions: true,
//         // <boolean> invoke formatters without executing steps
//         dryRun: false,
//         // <boolean> abort the run on first failure
//         failFast: false,
//         // <boolean> Enable this config to treat undefined definitions as
//         // warnings
//         ignoreUndefinedDefinitions: false,
//         // <string[]> ("extension:module") require files with the given
//         // EXTENSION after requiring MODULE (repeatable)
//         name: [],
//         // <boolean> hide step definition snippets for pending steps
//         snippets: true,
//         // <boolean> hide source uris
//         source: true,
//         // <string[]> (name) specify the profile to use
//         profile: [],
//         // <string[]> (file/dir) require files before executing features
//         require: [
//            'test/steps/given.js',
//             'test/steps/then.js',
//             'test/steps/when.js',
//             'test/steps_cross/given.js',
//             'test/steps_cross/then.js',
//             'test/steps_cross/when.js',
//             // Or search a (sub)folder for JS files with a wildcard
//             // works since version 1.1 of the wdio-cucumber-framework
//             // './src/**/*.js',
//         ],
//         // <string> specify a custom snippet syntax
//         snippetSyntax: undefined,
//         // <boolean> fail if there are any undefined or pending steps
//         strict: true,
//         // <string> (expression) only execute the features or scenarios with
//         // tags matching the expression, see
//         // https://docs.cucumber.io/tag-expressions/
//         tagExpression: 'not @Pending',
//         // <boolean> add cucumber tags to feature or scenario name
//         tagsInTitle: false,
//         // <number> timeout for step definitions
//         timeout: 180000,
//         compiler: [],
//         format: ['pretty'],
//         colors: true,
//         tags: [],
//     },
    
//     ...hooks,

    
// };
