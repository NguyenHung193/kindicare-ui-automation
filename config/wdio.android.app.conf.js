const { join } = require('path');
const path = require('path');
const { config } = require('./wdio.shared.conf');
const AndroidInfo = require('./android.info');

// ============
// Specs
// ============
// config.cucumberOpts.require = ['test/Steps/*.steps.js', ];

// ============
// Capabilities
// ============
// For all capabilities please check
// http://appium.io/docs/en/writing-running-appium/caps/#general-capabilities
config.capabilities = [
    {
        // The defaults you need to have in your config
        port: 4723,
        platformName: 'Android',
        noReset: true,
        fullReset: false,
        maxInstances: 1,
        automationName: 'uiautomator2',
        // uiid: "emulator-5554",
        name: "Bi2",
        appPackage: "com.linkedsite.mobile",
        appActivity: ".MainActivity",
        skipLogcatCapture: true,
        // metadata: {
        //     device: 'Samsung Galaxy S7 Edge',
        //     platform: {
        //     name: 'android',
        //     version: '8.0'
        //     }
        //     },
        deviceName: AndroidInfo.deviceName(),
        platformVersion: AndroidInfo.platFormVersion(),
        app: path.resolve(`./app/${AndroidInfo.appName()}`),
        newCommandTimeout: 300,
        // isHeadless: true,
        // 'app:isHeadless': true,
        // // For W3C the appium capabilities need to have an extension prefix
        // // http://appium.io/docs/en/writing-running-appium/caps/
        // // This is `appium:` for all Appium Capabilities which can be found here
        // 'appium:chromedriverExecutableDir':
        //     '<PATH TO CHROME DRIVER>',
        // 'appium:deviceName': 'emulator-5554',
        // 'appium:platformVersion': '8.0',
        // 'appium:orientation': 'PORTRAIT',
        // // `automationName` will be mandatory, see
        // // https://github.com/appium/appium/releases/tag/v1.13.0
        // 'appium:automationName': 'UiAutomator2',
        // // The path to the app
        // 'appium:app': join(
        //     process.cwd(),
        //     './apps/Android-NativeDemoApp-0.2.1.apk',
        // ),
        // // Read the reset strategies very well, they differ per platform, see
        // // http://appium.io/docs/en/writing-running-appium/other/reset-strategies/
        // // With these settings the application will not be closed between tests
        // 'appium:noReset': true,
        // 'appium:fullReset': false,
        // 'appium:dontStopAppOnReset': true,
        // 'appium:newCommandTimeout': 240,
        
        // "platformName": "Android",
        // "automationName": "UiAutomator2",
        // "uiid": "emulator-5554",
        // "appPackage": "com.enouvo.smartos",
        // "appActivity": ".MainActivity"
    },
];

exports.config = config;
