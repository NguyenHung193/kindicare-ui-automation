const shell = require("shelljs")
const video = require('wdio-video-reporter');
var cp = require('child_process');
var sleep = require('sleep');
const multiremote  = require ('webdriverio');
const AndroidInfo = require('./android.info');
const path = require('path');
// const ReportAggregator = require('@rpii/wdio-html-reporter').ReportAggregator ;
// const HtmlReporter = require('@rpii/wdio-html-reporter').HtmlReporter ;
// const log4js = require('@log4js-node/log4js-api');
// var rimraf = require("rimraf");
// var fs = require('fs');
// const { generate } = require('multiple-cucumber-html-reporter');
// const { removeSync } = require('fs-extra');
// const { hooks } = require('../test/support/hooks');

exports.config = {
    // ====================
    // Runner and framework
    // Configuration
    // ====================
    host: 'localhost',
    path: '/wd/hub/',
    port: 4723,
    mochaOpts: {
        ui: 'bdd',
        timeout: 60000
    },
    maxInstances: 1,
    capabilities: 
        {
    //     mychromeBrowser: {
    //         capabilities: {
    //     browserName: 'chrome',
    //     port: 4723,
    //     // If outputDir is provided WebdriverIO can capture driver session logs
    //     // it is possible to configure which logTypes to include/exclude.
    //     // excludeDriverLogs: ['*'], // pass '*' to exclude all driver session logs
    //     // excludeDriverLogs: ['bugreport', 'server'],
    //     'goog:chromeOptions': {
    //         // to run chrome headless the following flags are required
    //         // (see https://developers.google.com/web/updates/2017/04/headless-chrome)
    //         args: ['--disable-gpu','--no-sandbox', '--window-size=1920,1080'],
    //         },

    //     // maxInstances: 1,
    //     acceptInsecureCerts: true,
    //     // metadata: {
    //     //     device: 'Samsung Galaxy S7 Edge',
    //     //     platform: {
    //     //     name: 'android',
    //     //     version: '8.0'
    //     //     }
    //     //     },
    //     // deviceName: 'Samsung Galaxy S',
    //     // platformName: "Android",
    //     // automationName: "UiAutomator2",
    //     // uiid: "emulator-5554",
    //     // "appPackage": "com.enouvo.smartos",
    //     // "appActivity": ".MainActivity",
    //     // browserName: "S9",
    //     // "browserName": "S9",
    //     // "deviceName": "Samsung",
    //     // metadata: {
    //     //     device: 'Samsung Galaxy S7 Edge',
    //     //     platform: {
    //     //     name: 'android',
    //     //     version: '8.0'
    //     //     }
    //     //     }
    //     // 'newCommandTimeout': "2000",
    //     // "newCommandTimeout": 60000000,
    //   }},
    //   myAppium: {
    //     capabilities: {
    //     // The defaults you need to have in your config
    //     port: 4723,
    //     platformName: 'Android',
    //     noReset: true,
    //     fullReset: false,
    //     maxInstances: 1,
    //     automationName: 'uiautomator2',
    //     uiid: "emulator-5554",
    //     appPackage: "com.enouvo.smartos",
    //     appActivity: ".MainActivity",
    //     // metadata: {
    //     //     device: 'Samsung Galaxy S7 Edge',
    //     //     platform: {
    //     //     name: 'android',
    //     //     version: '8.0'
    //     //     }
    //     //     },
    //     deviceName: AndroidInfo.deviceName(),
    //     platformVersion: AndroidInfo.platFormVersion(),
    //     app: path.resolve(`./app/${AndroidInfo.appName()}`)
    //     // // For W3C the appium capabilities need to have an extension prefix
    //     // // http://appium.io/docs/en/writing-running-appium/caps/
    //     // // This is `appium:` for all Appium Capabilities which can be found here
    //     // 'appium:chromedriverExecutableDir':
    //     //     '<PATH TO CHROME DRIVER>',
    //     // 'appium:deviceName': 'emulator-5554',
    //     // 'appium:platformVersion': '8.0',
    //     // 'appium:orientation': 'PORTRAIT',
    //     // // `automationName` will be mandatory, see
    //     // // https://github.com/appium/appium/releases/tag/v1.13.0
    //     // 'appium:automationName': 'UiAutomator2',
    //     // // The path to the app
    //     // 'appium:app': join(
    //     //     process.cwd(),
    //     //     './apps/Android-NativeDemoApp-0.2.1.apk',
    //     // ),
    //     // // Read the reset strategies very well, they differ per platform, see
    //     // // http://appium.io/docs/en/writing-running-appium/other/reset-strategies/
    //     // // With these settings the application will not be closed between tests
    //     // 'appium:noReset': true,
    //     // 'appium:fullReset': false,
    //     // 'appium:dontStopAppOnReset': true,
    //     // 'appium:newCommandTimeout': 0,
        
    //     // "platformName": "Android",
    //     // "automationName": "UiAutomator2",
    //     // "uiid": "emulator-5554",
    //     // "appPackage": "com.enouvo.smartos",
    //     // "appActivity": ".MainActivity"
    // }}
},
    runner: 'local',
    framework: 'cucumber',
    sync: true,
    logLevel: 'warn',
    deprecationWarnings: false,
    outputDir: './test-report/output',
    app: '',
    bail: 0,
    baseUrl: 'http://the-internet.herokuapp.com',
    waitforTimeout: 6000,
    connectionRetryTimeout: 90000,
    connectionRetryCount: 3,
    
    reporters: [
        // [video, {
        //     saveAllVideos: true,       // If true, also saves videos for successful test cases
        //     videoSlowdownMultiplier: 20, // Higher to get slower videos, lower for faster videos [Value 1-100]
        //   }],
        'spec',
        // [
        //     'allure',
        //     {
        //         outputDir: './allure-report/allure-result/',
        //         disableWebdriverStepsReporting: false,
        //         disableWebdriverScreenshotsReporting: false,
        //         useCucumberStepReporter: true,
        //     },
        // ],
        
        // 'cucumberjs-json',

        // // OR like this if you want to set the folder and the language
        // [ 'cucumberjs-json', {
        //         jsonFolder: './test-report/cucumber-report',
        //         language: 'en',
        //     },
        // ],
    ],
    cucumberOpts: {
        requireModule: ['@babel/register'],
        backtrace: true,
        compiler: [],
        dryRun: false,
        failFast: false,
        format: ['pretty'],
        colors: true,
        snippets: true,
        source: true,
        profile: [],
        strict: false,
        tags: [],
        timeout: 100000,
        ignoreUndefinedDefinitions: false,
        tagExpression: 'not @skip',
        // require: ['test/Steps/login'],
        require: [
            'test/steps/given.js',
            'test/steps/then.js',
            'test/steps/when.js',
            // 'test/steps_cross/given.js',
            // 'test/steps_cross/then.js',
            // 'test/steps_cross/when.js',
            // Or search a (sub)folder for JS files with a wildcard
            // works since version 1.1 of the wdio-cucumber-framework
            // './src/**/*.js',
        ],
    },

    // ====================
    // Appium Configuration
    // ====================
    services: [
        // ['selenium-standalone', {
        //     logPath: 'logs',
        //     installArgs: { 
        //         chrome: { version: 'latest' }, 
        //         drivers : {
        //             chrome : {
        //                 version : "latest",
        //             }
        //         }
        // }, // drivers to install
        //     args: { 
        //         chrome: { version: 'latest' },
        //         drivers : {
        //             chrome : {
        //                 version : "latest",
        //             },
        //         },
        //         seleniumArgs: ['-host', '127.0.0.1','-port', '4723']
        //     } // drivers to use
        // }],

        ['appium',{
        command: 'appium',
        // relaxedSecurity: true,
        // logPath: "./" ,
        log: true,
        // port: 4723,
        args: {
        // For options see
        // https://github.com/webdriverio/webdriverio/tree/master/packages/wdio-appium-service
        // If you need a logs from appium server, make log equal true.
        // relaxedSecurity: true,
        // newCommandTimeout: 240,
        // logPath: "./" ,
        // log: true,
        // args: 
            // port: 4723,
            // platformName: "Android",
            // automationName: "UiAutomator2",
            // uiid: "emulator-5554",
            // appPackage: "com.linkedsite.mobile",
            // appActivity: ".MainActivity",
        //     // port: 4723
        //     // For arguments see
        //     // https://github.com/webdriverio/webdriverio/tree/master/packages/wdio-appium-service
        // ,
        
    }
    }]
],

    

    // ====================
    // Some hooks
    // ====================
    
    //This code is responsible for taking the screenshot in case of error and attaching it to the report
    afterStep(uri, feature, scenario) {
        //  if (scenario.error) {
        //      driver.takeScreenshot();
        //  }
     },
    //  afterTest: function (test) {
    //     const path = require('path');
    //     const moment = require('moment');

    //     // if test passed, ignore, else take and save screenshot.
    //     if (test.passed) {
    //         return;
    //     }
    //     const timestamp = moment().format('YYYYMMDD-HHmmss.SSS');
    //     const filepath = path.join('reports/html-reports/screenshots/', timestamp + '.png');
    //     browser.saveScreenshot(filepath);
    //     process.emit('test:screenshot', filepath);
    // }, 
    // afterStep: function (test, context, { error, result, duration, passed, retries }) {
    //     if (error) {
    //         driver.takeScreenshot();
    //     }
    //   },
    onPrepare : function () {
      
    // cp.exec('appium --port 4723',
    // (error, stdout, stderr) => {
    //     console.log(stdout);
    //     console.log(stderr);
    //     if (error !== null) {
    //         console.log(`exec error: ${error}`);
    //     }
    // });
    // removeSync('allure-report');
    // cp.exec('node deleteReport.js',
    // (error, stdout, stderr) => {
    //     console.log(stdout);
    //     console.log(stderr);
    //     if (error !== null) {
    //         console.log(`exec error: ${error}`);
    //     }
    // });
    // shell.exec('node deleteReport.js')
    // sleep.sleep(5)
    // var dir = 'test-report';

    // if (!fs.existsSync(dir)){
    //     fs.mkdirSync(dir);
    // }
    
    // cp.exec('sh startEmulator.sh',
    // (error, stdout, stderr) => {
    //     console.log(stdout);
    //     console.log(stderr);
    //     if (error !== null) {
    //         console.log(`exec error: ${error}`);
    //     }
    // });
        sleep.sleep(180)
        // shell.exec('adb -s emulator-5554 install /Users/hungnguyen/Downloads/smartBooking.apk')
        console.log("Start testing.....");
        sleep.sleep(10)
        // browser.reset()
        // sleep.sleep(10)
        shell.exec('adb start-server')
        console.log("Start server.....");

        // let reportAggregator = new ReportAggregator({
        //     outputDir: './reports/html-reports/',
        //     filename: 'master-report.html',
        //     reportTitle: 'Master Report',
        //     // browserName : browser.capabilities.browserName,
        //     // to use the template override option, can point to your own file in the test project:
        //     // templateFilename: path.resolve(__dirname, '../template/wdio-html-reporter-alt-template.hbs')
        // });
        // reportAggregator.clean() ;
 
        // global.reportAggregator = reportAggregator;
      
      },

    onComplete :  function () {
    //     cp.exec('adb shell reboot -p',
    // (error, stdout, stderr) => {
    //     console.log(stdout);
    //     console.log(stderr);
    //     if (error !== null) {
    //         console.log(`exec error: ${error}`);
    //     }
    // });
    // cp.exec('adb kill-server',
    // (error, stdout, stderr) => {
    //     console.log(stdout);
    //     console.log(stderr);
    //     if (error !== null) {
    //         console.log(`exec error: ${error}`);
    //     }
    // });

    //    shell.exec('adb kill-server')
    //    driver.deleteSession();
       console.log("Start remove package.....");
    //    shell.exec('adb uninstall com.linkedsite.mobile')
    //    console.log("Start remove package.....1");
    //    shell.exec('adb uninstall io.appium.uiautomator2.server')
    //    console.log("Start remove package.....2");
    //    shell.exec('adb uninstall io.appium.uiautomator2.server.test')
    //    console.log("Start remove package.....3");
    //    shell.exec('adb uninstall io.appium.unlock')
    //    console.log("Start remove package.....4");
    //    shell.exec('adb uninstall io.appium.settings')
    //    console.log("Start remove package.....5");
    //    shell.exec('adb shell reboot -p')
    //    console.log("Start remove package.....6");
    //    shell.exec('adb shell reboot -p')
       shell.exec('adb kill-server')
    //    console.log("Start remove package.....7");
    //    shell.exec('killall node')
    //    cp.exec('sh endEmulator.sh',
    // (error, stdout, stderr) => {
    //     console.log(stdout);
    //     console.log(stderr);
    //     if (error !== null) {
    //         console.log(`exec error: ${error}`);
    //     }
    // });

        
    console.log("End testing...");

    // (async () => {
    //     await global.reportAggregator.createReport();
    // })();
    },

    beforeScenario: function (uri, feature, scenario, sourceLocation, context) {
        sleep.sleep(5)
        browser.reset()
        sleep.sleep(5)
    },

    afterFeature: function (uri, feature, scenarios) {
        
    },

    beforeFeature: function (uri, feature, scenarios) {
        // shell.exec('adb -s emulator-5554 install /Users/hungnguyen/Downloads/newSrc/android/app/build/outputs/apk/releaseStaging/app-releaseStaging.apk')
        console.log("Installing.....");
        // sleep.sleep(5)
        // sleep.sleep(5)
    },

    specs: ['test/features/**/*.feature'],

    // ...hooks,
};
