class AndroidInfo {
    static deviceName() {
        return 'Bi2'; // pass the udid or devicename
    }

    static platFormVersion() {
        return '7.1.1'; // pass the platform version
    }

    static appName() {
        return 'app-releaseSIT.apk'; // pass the apk name
    }

    static appPackage() {
        return 'com.linkedsite.mobile'; // pass the apk name
    }
}

module.exports = AndroidInfo;
