const report = require('multiple-cucumber-html-reporter');

report.generate({
	// Required
	// This part needs to be the same path where you store the JSON files
	// default = '.tmp/json/'
	jsonDir: '.tmp/json/',
	reportPath: '.tmp/report/abc/',
	displayDuration: false,
	durationInMS: true,
	pageTitle: 'Acciona Mobile Automation',
	openReportInBrowser: true,
	reportName: 'Report by Enouvo',
	displayReportTime: true,
	customData: {
		title: 'Run info',
		data: [
			{label: 'Project', value: 'Acciona Mobile'},
			{label: 'Release', value: '1.0.5'},
		]
	},
	customMetadata: true,
	metadata:{
		browser: {
			name: 'chrome',
			version: '60'
		},
		device: 'Local test machine',
		platform: {
			name: 'ubuntu',
			version: '16.04'
		}
	},
	// for more options see https://github.com/wswebcreation/multiple-cucumber-html-reporter#options
  });